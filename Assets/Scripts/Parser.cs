using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using System.IO;
using Newtonsoft.Json;


public class Parser : MonoBehaviour
{
    private Container container;

    private const string questsFileName = "quest_step_task";
    private const string nodesFileName = "edge_task";


    public string GetPath(string fileName)
    {
        string path = Application.dataPath + "/Resources/";
        return path + fileName + ".json";
    }

    private void Awake()
    {
        container = FindObjectOfType<Container>();
    }

    public void ReadAllData()
    {
        ReadQuestData();
        ReadNodesData();
    }

    private void ReadQuestData()
    {
        string json = File.ReadAllText(GetPath(questsFileName));
        container.questData = JsonConvert.DeserializeObject<List<QuestData>>(json);
    }

    private void ReadNodesData()
    {
        string json = File.ReadAllText(GetPath(nodesFileName));
        container.nodeData = JsonConvert.DeserializeObject<List<NodeData>>(json);
    }
}
