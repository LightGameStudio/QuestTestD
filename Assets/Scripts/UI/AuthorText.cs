using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using TMPro;

public class AuthorText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI label;

    public void SetText(string text)
    {
        label.text = text;
    }
}
