using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelUI : MonoBehaviour
{
    public AuthorText authorText;

    private NovelRunner novelRunner;

    private void Awake()
    {
        novelRunner = FindObjectOfType<NovelRunner>();
        authorText = FindObjectOfType<AuthorText>();
    }

    public void ClickNext()
    {
        novelRunner.MoveToNextSection();
    }
}
