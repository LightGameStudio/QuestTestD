using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;

[System.Serializable]
public class Card
{
    public int id { get; set; }
    public int queststep_id { get; set; }
    public Image image { get; set; }
    public DateTime updated_at { get; set; }
}

public class Image
{
    public string file_id { get; set; }
}

public class QuestData
{
    public string description { get; set; }
    public string choice_description { get; set; }
    public int id { get; set; }
    public List<Visualisation> visualisations { get; set; }
    public Card card { get; set; }
}

public class Visualisation
{
    public string title { get; set; }
    public string description { get; set; }
    public int id { get; set; }
}

