using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;

[System.Serializable]
public class Container : MonoBehaviour
{
    public List<QuestData> questData;
    public List<NodeData> nodeData;
    public List<Section> sections = new List<Section>();

    private const int ExitNodeId = 9999;

    public void FormSections()
    {
        List<QuestData> sortedQuestData = questData.OrderBy(o => o.id).ToList();
        questData = sortedQuestData;

        for (int i = 0; i < questData.Count; i++)
        {
            List<NodeData> outNodes = new List<NodeData>();
            List<int> outNodesId = new List<int>();

            // Set Out Nodes

            if (i < nodeData.Count)
            {
                outNodes = nodeData.Where(s => s.source_id == nodeData[i].target_id).ToList();
                if (outNodes.Count > 0)
                {
                    foreach (var nd in outNodes)
                    {
                        outNodesId.Add(nd.source_id);
                    }
                }
            }
            else
            {
                NodeData node = new NodeData(questData[i].id, ExitNodeId);
                outNodes.Add(node);
            }

            // Set In Node
            NodeData inNode;

            if (i < nodeData.Count)
            {
                inNode = nodeData[i];
            }
            else
            {
                inNode = new NodeData(0, questData[i].id);
            }

            Section section;
            section = new Section(questData[i], inNode.source_id, outNodesId);
            sections.Add(section);
        }
    }
}
