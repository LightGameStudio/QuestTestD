using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class NodeData
{
    public int source_id { get; set; }
    public int target_id { get; set; }
    public object id { get; set; } // Always null in JSON

    public NodeData(int from, int to)
    {
        source_id = from;
        target_id = to;
    }
}