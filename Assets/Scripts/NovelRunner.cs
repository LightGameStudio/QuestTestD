using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NovelRunner : MonoBehaviour
{
    private Parser parser;
    private Container container;
    private int currentNodeId;
    private int currentId = 0;
    private Section CurrentSection => container.sections[currentId];
    private LevelUI levelUI;

    private void Awake()
    {
        parser = FindObjectOfType<Parser>();
        container = FindObjectOfType<Container>();
        levelUI = FindObjectOfType<LevelUI>();
    }

    private void Start()
    {
        parser.ReadAllData();
        container.FormSections();
        Run();
    }

    private void Run()
    {
        currentNodeId = container.sections[0].inNode;

        RunCurrentSection();
    }

    public void MoveToNextSection()
    {
        currentId++;
        MoveToSection(currentId);
    }

    private void MoveToSection(int id)
    {
        RunSection(id);
    }

    private void RunCurrentSection()
    {
        RunSection(currentId);
    }

    private void RunSection(int id)
    {
        levelUI.authorText.SetText(container.sections[id].questData.description);
        Debug.Log("Node id:" + container.sections[id].inNode);
    }
}
