using System.Collections.Generic;

public class Section
{
    public QuestData questData;
    public int inNode;
    public List<int> outNode = new List<int>();

    public Section(QuestData questData, int inNode, List<int> outNode = null)
    {
        this.questData = questData;
        this.inNode = inNode;
        if (outNode != null)
        {
            this.outNode = outNode;
        }
    }
}